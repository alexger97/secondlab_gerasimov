﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSTECParser
{
   public class Threat
    {public static  int num=0;
        public  int Id { get; private set; }
        public string ThreatName { get; set; }
        public string ThreatDescription { get; set; }
        public string ThreatSource { get; set; }
        public string ThreatObject { get; set; }
        public string Сonfidentiality { get; set; }
        public string Integrity { get; set; }
        public string Availability { get; set; }

        public Threat(int  id, string threatName, string threatDescription, string threatSource, 
            string threatObject, string сonfidentiality, string integrity, string availability)
        {
            Id = id;
            ThreatName = threatName;
            ThreatDescription = threatDescription;
            ThreatSource = threatSource;
            ThreatObject = threatObject;
            if (сonfidentiality.Contains("1")) { 
            Сonfidentiality = "Да";}
            else { Сonfidentiality = "Нет"; }
            if (integrity.Contains("1"))
            {
                Integrity = "Да";
            }
            else { Integrity = "Нет"; }
            if (availability.Contains("1"))
            {
                Availability = "Да";
            }
            else { Availability = "Нет"; }
            //Availability = availability;
            Threat.num += 1;
        }

        public override bool Equals(object obj)
        {
            var threat = obj as Threat;
            return threat != null &&
                   Id == threat.Id &&
                   ThreatName == threat.ThreatName &&
                   ThreatDescription == threat.ThreatDescription &&
                   ThreatSource == threat.ThreatSource &&
                   ThreatObject == threat.ThreatObject &&
                   Сonfidentiality == threat.Сonfidentiality &&
                   Integrity == threat.Integrity &&
                   Availability == threat.Availability;
        }

        public override int GetHashCode()
        {
            var hashCode = 1156073086;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ThreatName);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ThreatDescription);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ThreatSource);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ThreatObject);
            hashCode = hashCode * -1521134295 + Сonfidentiality.GetHashCode();
            hashCode = hashCode * -1521134295 + Integrity.GetHashCode();
            hashCode = hashCode * -1521134295 + Availability.GetHashCode();
            return hashCode;
        }
        public override string ToString()
        {
            return this.Id + this.ThreatName + this.ThreatDescription + this.ThreatSource + this.ThreatObject + this.Сonfidentiality + this.Integrity + this.Availability;
        }
        public static string[] toarrr(Threat x)
        { string[] c= new string[8];
          
            c[0] = x.Id.ToString();
            c[1] = x.ThreatName;
            c[2] = x.ThreatDescription;
            c[3] = x.ThreatSource;
            c[4] = x.ThreatObject;
            c[5] = x.Сonfidentiality;
            c[6] = x.Integrity;
            c[7] = x.Availability;
           /* for (int i = 0; i < c.Length; i++)
            {

                {
                    if (c.ElementAt(i).Contains("_x000d_"))
                    { c[i] = c.ElementAt(i).Replace("_x000d_", " "); }
                }
            }*/
            return c;

        }

    }
}
